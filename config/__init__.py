from __future__ import print_function
import os
from shutil import copyfile
from collections import OrderedDict
import yaml
import yaml.constructor
# TODO relative filenames?

settings = dict()


def _goto_parameter(names):
    """Returns the dictionary corresponding to the parameter object
    identified by the argument
    """
    def find_child(node, name):
        """Attempts to find a child node with the given name.
        Raises KeyError if no child with the given name exists
        """
        for elem in node['children']:
            if elem['name'] == name:
                return elem
        else:
            raise KeyError("'{}' is not a subparameter of '{}'"
                           .format(name, node['name']))
    # Begin at the parameter tree root
    node = settings['params']
    # Search down the tree
    for name in names:
        node = find_child(node, name)
    return node


def get_setting(setting):
    """Returns the value of a setting by name.
    """
    return settings[setting]


def get_parameters():
    """Retrieves the tree of all parameters.
    """
    return settings['params']


def get_parameter(*names):
    """Gets the value of a parameter.
    The names argument is a list that describes where the parameter is stored.
    """
    node = _goto_parameter(list(names))
    return node['value']


def set_parameter(names, value):
    """Sets a parameter in the configuration to the given value.
    The names argument is a list that describes where the parameter is stored.
    Does not update the value of this parameter in the GUI.
    Does not store the modified settings to disk.
    """
    node = _goto_parameter(names)
    node['value'] = value


# Loading settings
def load_settings(filename="config/config.yaml"):
    """Loads new settings from a file.
    """
    global settings
    if not os.path.isfile(filename):  # Config file does not exist
        copyfile("config/config.yaml.bak", filename)
    with open(filename, "r") as f:
        settings.update(yaml.load(f))
    print("Settings loaded")


# Saving settings
def save_settings(filename="config/config.yaml"):
    """Saves the current settings to a file.
    """
    global settings
    with open(filename, "w") as f:
        ordered_dump(ordered(settings), f, default_flow_style=False, indent=4)
    print("Settings saved")


priority = {
    # Important properties first
    'name': 0,
    'title': 1,
    'type': 2,
    'value': 3,
    'default': 4,
    # Secondary properties
    'enabled': 5,
    'expanded': 6,
    'limits': 7,
    'readonly': 8,
    'removable': 9,
    'renamable': 10,
    'siPrefix': 11,
    'strictNaming': 12,
    'suffix': 13,
    'tip': 14,
    'visible': 15,
    # Children last (maintain a readable tree structure)
    'children': 99,
}


def ordered(settings):
    """Transforms an unordered tree of settings into a recursively ordered
    tree of settings, for better display in the config file.
    """
    if isinstance(settings, dict):
        # dict: sort keys, recurse
        ordered_settings = OrderedDict()
        for key in sorted(settings.keys(), key=lambda s: priority.get(s, 50)):
            ordered_settings[key] = ordered(settings[key])
        return ordered_settings
    elif isinstance(settings, OrderedDict):
        # Do not mess with an existing ordering, only recurse
        ordered_settings = OrderedDict()
        for key in settings.keys():
            ordered_settings[key] = ordered(settings[key])
    elif isinstance(settings, (tuple, list)):
        # List or tuple: recurse, keep ordering
        return type(settings)(map(ordered, settings))
    else:
        # Simple value, does not need ordering
        return settings


# https://stackoverflow.com/questions/5121931/in-python-how-can-you-load-yaml-mappings-as-ordereddicts
def ordered_dump(data, stream=None, **kwargs):
    """Dumps an OrderedDict as if it were a normal dict, while preserving
    key order.
    """
    class OrderedDumper(yaml.Dumper):

        pass

    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            data.items())
    OrderedDumper.add_representer(OrderedDict, _dict_representer)
    return yaml.dump(data, stream, OrderedDumper, **kwargs)
