# -*- coding: utf-8 -*-

from __future__ import division
import time
import numpy as np
from PyQt4 import QtCore

SIG = QtCore.SIGNAL


class BaseWorker(QtCore.QThread):
    def __init__(self):
        super(BaseWorker, self).__init__()
        self.aborting = False

    def abort(self):
        self.aborting = True

    def aborted(self):
        return self.aborting

    def __del__(self):
        self.wait()


class SimpleWorker(BaseWorker):
    def __init__(self, nb_measurements):
        super(SimpleWorker, self).__init__()
        self.nb = nb_measurements

    def run(self):
        for (i, theta) in enumerate(np.linspace(0, 4*np.pi, self.nb)):
            if self.aborting:
                return
            self.emit(SIG("progress(int)"), i * 100 // self.nb)
            time.sleep(0.02)
            noise = np.random.rand() - 0.5
            data = 2 * np.sin(theta) + noise
            self.emit(SIG("dataBatch(float, float)"), theta, data)
        self.emit(SIG("progress(int)"), 100)


class HRSWorker(BaseWorker):
    """Performs HRS measurements.
    """
    def __init__(self, lockin=None, servo=None, points=None, measurements=None,
                 range=None):
        super(HRSWorker, self).__init__()
        assert lockin is not None and servo is not None
        self.lockin = lockin
        self.lockin.setup()
        self.servo = servo
        assert type(points) == int and points > 0
        self.points = points
        assert type(measurements) == int and measurements > 0
        self.measurements = measurements
        rstart, rend = range
        assert rstart < rend
        self.rstart = rstart
        self.rend = rend

    def run(self):
        for (pnum, th) in enumerate(np.linspace(self.rstart, self.rend,
                                                self.points)):
            self.emit(SIG("status(QString)"),
                      "Moving servo to angle {}...".format(th))
            self.servo.goto(th)
            for mnum in xrange(self.measurements):
                if self.aborting:
                    return
                progress = (mnum + pnum / self.measurements) / self.points
                self.emit(SIG("progress(int)"), int(100 * progress))
                time.sleep(0.3)
                a = self.lockin.query(3)
                time.sleep(0.2)
                b = self.lockin.query(4)
                # a, b = self.lockin.query_multiple(1, 2)
                print("a: {}".format(repr(a)))
                print("b: {}".format(repr(b)))
                self.emit(SIG("dataBatch(float, float)"),
                          float(a), float(b))
        self.emit(SIG("progress(int)"), 100)


class SweepWorker(BaseWorker):
    """Performs a 360 degrees sweep to discover the minimum intensity angle.
    """
    def __init__(self, lockin, servo, points, measurements):
        super(SweepWorker, self).__init__()
        assert lockin is not None and servo is not None
        self.lockin = lockin
        self.lockin.setup()
        self.servo = servo
        assert isinstance(points, int) and points > 0
        self.points = points
        assert isinstance(measurements, int) and measurements > 0
        self.measurements = measurements

    def run(self):
        pos = round(self.servo.position(), 2)
        self.servo.goto(0, wait=False)
        while self.servo.status().moving:  # Wait until the servo stopped
            self.emit(SIG("status(QString)"),
                      "Moving to home position (now at {})...".format(pos))
            time.sleep(0.1)
            pos = round(self.servo.position(), 2)

        for (pnum, theta) in enumerate(np.linspace(0, 360, self.points)):
            self.emit(SIG("status(QString)"),
                      "Moving servo to {} degrees...".format(theta))
            if self.aborting:
                return
            self.servo.goto(theta)
            self.emit(SIG("status(QString)"), "Servo is at {}."
                      .format(round(self.servo.position(), 2)))

            for mnum in xrange(self.measurements):
                if self.aborting:
                    return
                progress = (mnum + pnum / self.measurements) / self.points
                self.emit(SIG("progress(int)"), int(100 * progress))
                time.sleep(0.3)
                y = float(self.lockin.query(3))
                self.emit(SIG("dataBatch(float, float)"), theta, y)

        self.emit(SIG("progress(int)"), 100)


class LockInSponge(BaseWorker):
    """Reads everything from the lock-in.
    """
    def __init__(self, lockin):
        super(LockInSponge, self).__init__()
        self.lockin = lockin
        self.lockin.setup()

    def run(self):
        while not self.aborting:
            # keep running indefinitely until the user exits
            l = []
            l.extend(self.lockin.query_multiple(*range(1, 7)))  # 1 : 6
            l.extend(self.lockin.query_multiple(*range(7, 12)))  # 7 : 11
            self.emit(SIG("dataBatch(PyQt_PyObject)"), l)
            time.sleep(0.5)  # update twice every second
