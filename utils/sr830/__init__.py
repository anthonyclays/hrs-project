#!/usr/bin/env python
# -*- coding: utf-8 -*-

"Stanford Research Systems SR830 controller and utilities."

from serial import Serial, EIGHTBITS, PARITY_NONE, STOPBITS_ONE

from constants import *  # NOQA

__all__ = ["SR830"].append(filter(lambda name: name[0] != '_', dir(constants)))


class SR830(object):
    """Controller for a Stanford Research Systems,
    model SR830 DSP Lock-In Amplifier.
    Communicates via a USB -> RS232 converter.
    """
    def __init__(self, port=None):
        """Creates a new SR830 object.
        If no port is provided, will attempt to discover connected SR830s.
        """
        if port is None:
            raise Exception("No serial connection provided!")
        self.port = Serial(port, baudrate=9600, bytesize=EIGHTBITS,
                           parity=PARITY_NONE, stopbits=STOPBITS_ONE,
                           timeout=0.5)
        # Test connection
        assert self.identify().startswith('Stanford_Research_Systems')

    def _read_line(self, eol=b'\r'):
        """Reads from the serial port until the eol string is detected,
        or until no more data is read (timeout).
        """
        l = len(eol)
        buf = bytearray()
        while True:
            c = self.port.read(1)
            if c:
                buf += c
                if buf[-l:] == eol:
                    break
            else:
                break
        return str(buf)

    def _send_command(self, cmd):
        """Sends a command to the device. Does not wait for output."""
        assert '?' not in cmd
        self.port.write(cmd + '\n')
        self.port.flushOutput()

    def _read_output(self, cmd, check_lias=True):
        """Sends a query to the device, and returns its reply."""
        if check_lias:
            self.port.flushInput()
            lias = self.lias() & 0xf
            if lias != 0:
                problems = []
                if 0b1 & lias:
                    problems.append("RESERVE or INPUT overload")
                if 0b01 & lias:
                    problems.append("FILTR overload")
                if 0b001 & lias:
                    problems.append("OUTPT overload")
                if 0b0001 & lias:
                    problems.append("Reference unlock")
                raise Exception(', '.join(problems))
        self.port.flushInput()
        assert '?' in cmd
        self.port.write(cmd + '\n')
        self.port.flushOutput()
        res = self._read_line().rstrip()
        while len(res) == 0:
            print("No output :/")
            self.port.write(cmd + '\n')
            self.port.flushOutput()
            res = self._read_line().rstrip()
        return res

    def reset(self):
        """Resets the device to its default configuration"""
        self._send_command('*RST')

    def identify(self):
        """Queries the identification string of the device (*IDN)."""
        return self._read_output('*IDN?')

    def get_phase(self):
        """Queries the phase shift (in degrees)
        :rtype: float
        """
        return float(self._read_output('PHAS?'))

    def set_phase(self, phase):
        """Sets the phase shift (in degrees).
        :type phase: float
        """
        self._send_command('PHAS {}'.format(float(phase)))

    def get_freq_source(self):
        """Queries the frequency source.
        0 = Internal reference source
        1 = External reference source
        :rtype: int
        """
        return int(self._read_output('FMOD?'))

    def set_freq_source(self, freq_src):
        """Sets the frequency source of the device.
        0 = Internal reference source
        1 = External reference source
        :type freq_src: int
        """
        self._send_command('FMOD {}'.format(int(freq_src)))

    def get_frequency(self):
        """Queries the internal signal generator frequency.
        :rtype: float
        """
        return float(self._read_output('FREQ?'))

    def set_frequency(self, freq):
        """Sets the frequency of the internal signal generator.
        :type freq: float
        """
        freq = float(freq)
        self._send_command('FREQ {}'.format(freq))

    def get_output_amplitude(self):
        """Queries the output amplitude of the internal signal generator.
        :rtype: float
        """
        return float(self._read_output('SLVL?'))

    def set_output_amplitude(self, ampl):
        """Sets the output amplitude of the internal signal generator in volts.
        The amplitude must be between 0.004 and 5.0.
        :type ampl: float
        """
        ampl = float(ampl)
        if 0.004 <= ampl <= 5.0:
            self._send_command('SLVL {}'.format(ampl))
        else:
            raise Exception('Amplitude out of bounds for signal generator: {}V'
                            .format(ampl))

    def get_grounding(self):
        """Queries the input shield grounding.
        0 = Float
        1 = Ground
        :rtype: int
        """
        return self._read_output('IGND?')

    def set_grounding(self, grounding):
        """Sets the input shield grounding.
        0 = Float
        1 = Ground
        :type grounding: int
        """
        self._send_command('IGND {}'.format(int(grounding)))

    def get_sensitivity(self):
        """Queries the sensitivity.
        See constants.py
        :rtype: int
        """
        return int(self._read_output('SENS?'))

    def set_sensitivity(self, sens):
        """Sets the sensitivity.
        See constants.py
        :type sens: int
        """
        self._send_command('SENS {}'.format(int(sens)))

    def get_reserve_mode(self):
        """Queries the dynamic reserve mode.
        0 = High Reserve
        1 = Normal
        2 = Low Noise
        :rtype: int
        """
        return self._read_output('RMOD?')

    def set_reserve_mode(self, rmode):
        """Sets the dynamic reserve mode.
        0 = High Reserve
        1 = Normal
        2 = Low Noise
        :type rmode: int
        """
        self._send_command('RMOD {}'.format(int(rmode)))

    def get_time_constant(self):
        """Queries the time constant.
        See constants.py
        :rtype: int
        """
        return int(self._read_output('OFLT?'))

    def set_time_constant(self, tau):
        """Sets the time constant.
        See constants.py
        :type tau: int
        """
        self._send_command('OFLT {}'.format(int(tau)))

    def get_low_pass_filter_slope(self):
        """Queries the low pass filter slope.
        0 = 6dB
        1 = 12dB
        2 = 18dB
        3 = 24dB
        :rtype: int
        """
        return int(self._read_output('OFSL?'))

    def set_low_pass_filter_slope(self, slope):
        """Sets the low pass filter slope.
        0 = 6dB
        1 = 12dB
        2 = 18dB
        3 = 24dB
        :type slope: int
        """
        self._send_command('OFSL {}'.format(int(slope)))

    def get_channel_display(self, ch):
        """Queries the channel display and ratio for a channel.
        :type ch: int
        :rtype: (int, int)
        # TODO
        """
        output = self._read_output('DDEF? {}'.format(int(ch)))
        (display, ratio) = map(int, output.split(','))
        return display, ratio

    def set_channel_display(self, ch, display, ratio=0):
        """Sets the channel display for a channel.
        # TODO
        """
        self._send_command('DDEF {i} {j} {k}'.format(i=ch, j=display, k=ratio))

    def query(self, param):
        """Queries the value of X, Y, R or Theta
        :type param: int
        """
        param = int(param)
        assert 1 <= param <= 4
        return self._read_output('OUTP? {}'.format(param))

    def query_multiple(self, *params):
        """Queries multiple parameters at once."""
        assert 2 <= len(params) <= 6
        assert 1 <= min(params) and max(params) <= 11
        params = map(str, params)
        return self._read_output('SNAP? {}'.format(','.join(params))) \
            .split(',')

    def get_output_iface(self):
        """Queries the output interface and ratio for a channel.
        :rtype: int
        """
        return int(self._read_output('OUTX?'))

    def set_output_iface(self, iface):
        """Sets the output interface to RS232 (0) or GPIB (1).
        :type iface:
        """
        iface = int(iface)
        self._send_command('OUTX {}'.format(iface))

    def lias(self):
        self.port.flushInput()
        res = None
        while not res:
            self.port.write('lias?\n')
            self.port.flushOutput()
            res = self._read_line()
            try:
                res = int(res)
            except ValueError:
                res = None
                continue
        return res

    def setup(self):
        """Setup the device with some default configurations."""
        self.reset()
        self.set_output_iface(OutputInterface["RS232"])
        self.set_freq_source(ReferenceSource["External"])
        self.set_grounding(Grounding["Ground"])
        self.set_sensitivity(Sensitivity["1V"])
        self.set_reserve_mode(ReserveMode["Normal"])
        self.set_time_constant(TimeConstant["100ms"])
        self.set_low_pass_filter_slope(FilterSlope["24dB"])
