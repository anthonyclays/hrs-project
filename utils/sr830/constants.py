"Constants used in the SR830"

ReferenceSource = {
    "Internal": 0,
    "External": 1,
}

Grounding = {
    "Float": 0,
    "Ground": 1,
}

ReserveMode = {
    "HighReserve": 0,
    "Normal": 1,
    "LowNoise": 2,
}

FilterSlope = {
    "6dB": 0,
    "12dB": 1,
    "18dB": 2,
    "24dB": 3,
}

InputCoupling = {
    "AC": 0,
    "DC": 1,
}

TimeConstant = {
    "10mus": 0,
    "30mus": 1,
    "100mus": 2,
    "300mus": 3,
    "1ms": 4,
    "3ms": 5,
    "10ms": 6,
    "30ms": 7,
    "100ms": 8,
    "300ms": 9,
    "1s": 10,
    "3s": 11,
    "10s": 12,
    "30s": 13,
    "100s": 14,
    "300s": 15,
    "1ks": 16,
    "3ks": 17,
    "10ks": 18,
    "30ks": 19,
}

Sensitivity = {
    "2nV": 0,
    "5nV": 1,
    "10nV": 2,
    "20nV": 3,
    "50nV": 4,
    "100nV": 5,
    "200nV": 6,
    "500nV": 7,
    "1muV": 8,
    "2muV": 9,
    "5muV": 10,
    "10muV": 11,
    "20muV": 12,
    "50muV": 13,
    "100muV": 14,
    "200muV": 15,
    "500muV": 16,
    "1mV": 17,
    "2mV": 18,
    "5mV": 19,
    "10mV": 20,
    "20mV": 21,
    "50mV": 22,
    "100mV": 23,
    "200mV": 24,
    "500mV": 25,
    "1V": 26,
}

CH1 = 1
CH2 = 2

Display = {
    "XY": 0,
    "RTheta": 1,
    "XnYn": 2,
    "Aux13": 3,
    "Aux24": 4,
}

Ratio = {
    "None": 0,
    "Aux13": 1,
    "Aux24": 2,
}

OutputInterface = {
    "RS232": 0,
    "GPIB": 1,
}
