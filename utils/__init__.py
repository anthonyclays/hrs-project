import sys

from serial.tools.list_ports import comports

from pyAPT import TDC001
from sr830 import SR830


def connect_SR830():
    """Tries to connect to a SR830 Lock-in amplifier.
    If one isn't found, None is returned.
    """
    if sys.platform.lower() == 'win32':
        loc = 'COM1'
    else:  # linux
        for loc, desc, info in comports():
            if info == 'USB VID:PID=067b:2303':  # rs232 - serial converter
                break
        else:
            print 'SR830 not found!'
            return None
    return SR830(loc)


def connect_TDC001():
    """Tries to connect to a TDC001 servo controller.
    If one isn't found, None is returned.
    """
    import pylibftdi
    controllers = pylibftdi.Driver().list_devices()
    if not controllers:
        print("No servo controllers found!")
        return None
    else:
        manufacturer, desc, serial = controllers[0]
        print("Found {manufacturer} {desc}, S/N {serial}"
              .format(**locals()))
        pv_id = controllers[0][2]
        return TDC001(pv_id)
