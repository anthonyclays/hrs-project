#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import csv

import pyqtgraph as pg
from pyqtgraph.parametertree import Parameter
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np

# TODO relative imports
from view import Ui_MainWindow, Ui_ServoControl, Ui_LockInControl
from config import get_setting, load_settings, save_settings, \
    set_parameter, get_parameter, get_parameters
from utils import connect_SR830, connect_TDC001
from workers import SimpleWorker, HRSWorker, SweepWorker, LockInSponge

DEBUG = True

MBox = QtGui.QMessageBox
SIG = QtCore.SIGNAL

load_settings()


class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(QtGui.QMainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.plotWidget = self.ui.plotWidget

        # Create slot for worker thread
        self.worker = None

        # Create slot for measurement data and its description
        self.data_tags = None
        self.data = None

        # (Try to) connect peripherals
        self.servo = connect_TDC001()
        self.lockin = connect_SR830()

        # Setup parameter tree
        self.params = None
        set_parameter(['advanced', 'thetamax'], 0.0)
        self.load_parameters()
        self.params.sigTreeStateChanged.connect(self.param_tree_changed)
        # Setup parameter tree widget
        self.ui.paramTree.setParameters(self.params, showTop=False)
        self.ui.paramTree.setHeaderLabels(["Parameter", "Value"])

        # Setup menu actions
        self.ui.actionQuit.triggered.connect(self.quit)
        self.ui.actionExport.triggered.connect(self.export_measurements)
        self.ui.actionSaveParameters.triggered.connect(self.save_parameters)
        self.ui.actionLoadParameters.triggered.connect(self.load_parameters)
        self.ui.actionChooseExportDirectory.triggered.connect(self.choose_export_dir)  # NOQA
        self.ui.actionAbout.triggered.connect(self.show_about)
        self.ui.actionViewManual.triggered.connect(self.show_manual)

        # Setup toolbar actions
        self.ui.actionStartHRS.triggered.connect(self.experiment_HRS)
        self.ui.actionStartDepol.triggered.connect(self.experiment_depol)
        self.ui.actionPlotTest.triggered.connect(self.experiment_plot_test)
        self.ui.actionServoControl.triggered.connect(
                self.experiment_servo_control)
        self.ui.actionLockinTest.triggered.connect(self.experiment_lockin_test)
        self.ui.actionFullSweep.triggered.connect(self.experiment_full_sweep)
        # Add new toolbar actions before this line
        # and append them to the field `experiments` below.
        # They will then be impossible to launch while
        # another experiment is still running
        self.ui.actionAbortExperiment.triggered.connect(self.abort_experiment)
        self.experiments = [self.ui.actionStartHRS, self.ui.actionStartDepol,
                            self.ui.actionPlotTest, self.ui.actionServoControl,
                            self.ui.actionLockinTest, self.ui.actionFullSweep]
        if not DEBUG:
            self.ui.actionPlotTest.setVisible(False)

        # Setup status bar
        self.status = QtGui.QLabel()
        self.status.setAlignment(QtCore.Qt.AlignRight)
        self.status.setText("No experiment running.")
        self.statusBar().addPermanentWidget(self.status, 1)
        self.progress = QtGui.QProgressBar()
        self.progress.setRange(0, 100)
        self.progress.setValue(0)
        self.statusBar().addPermanentWidget(self.progress, 1)

        # Finalize window setup
        self.resize(*get_setting('window_size'))

    def param_tree_changed(self, root_param, changes):
        """Handles changes in the parameter tree.
        Writes the changes to the internal settings, but does not save them to
        disk.
        """
        def handle_tree_change(param, attribute, value):
            """Handles one parameter change in the tree.
            """
            if attribute == 'value':
                # The value of a parameter was edited
                param_path = self.params.childPath(param)
                set_parameter(param_path, value)
            elif attribute == 'activated':
                # A button was clicked
                self.exec_param_tree_action(param.name())
            else:
                print("Unknown attribute in a parameter tree change:")
                print("param = {p}, attribute = {attr}, value = {val}"
                      .format(p=param, attr=attribute, val=value))
                return
        for param, attribute, value in changes:
            handle_tree_change(param, attribute, value)

    # Menu actions
    def quit(self):
        """Quits the application.
        """
        # TODO check if worker thread is busy, check if settings changed?
        if self.worker is None or \
                self.warn("Worker thread busy",
                          "An experiment is currently running, interrupt?"):
            self.close()

    def export_measurements(self):
        """Exports the current measurements to disk.
        """
        if self.data is None or len(self.data) == 0:
            return self.error("No data", "No data to export.")
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Choose Export File', get_parameter('export', 'export_dir'))  # NOQA
        if not filename:
            return  # Canceled file select.
        with open(filename, 'w') as f:
            writer = csv.writer(f)
            if self.data_tags is not None:
                writer.writerow(self.data_tags)
            writer.writerows(self.data)

    def save_parameters(self):
        """Writes all changes to the parameter tree to disk.
        """
        print("Saving parameters to disk...")
        save_settings()

    def load_parameters(self):
        """Loads settings from disk and creates a parameter tree with
        the loaded parameters.
        """
        self.params = Parameter.create(**get_parameters())

    def choose_export_dir(self):
        """Shows a file dialog and sets the export directory if a new location
        is provided.
        """
        param_loc = ['export', 'export_dir']
        cur_dir = get_parameter(*param_loc)
        dialog = QtGui.QFileDialog()
        dialog.setFileMode(QtGui.QFileDialog.Directory)
        dialog.setOption(QtGui.QFileDialog.ShowDirsOnly)
        new_dir = dialog.getExistingDirectory(self, 'Choose Export Directory', cur_dir)  # NOQA
        new_dir = str(new_dir)
        if new_dir:
            set_parameter(param_loc, new_dir)
            self.set_gui_parameter(param_loc, new_dir)

    def show_about(self):
        """Shows an about dialog containing information about this program.
        """
        # TODO
        MBox.about(self, "About this program",
                   "This software was written by Anthony Clays.\n"
                   "Open source software and libraries used:\n"
                   "PyQt4\n"
                   "PyYaml\n"
                   "pyqtgraph\n"
                   "pyAPT\n"
                   "#TODO\n")

    def show_manual(self):
        """Shows the program manual.
        """
        # TODO
        print("Showing manual")

    # UTILITIES
    # Experiment utilities
    def start_experiment(self):
        """Sets up the toolbar when an experiment is starting.
        """
        # Enable experiment abort
        self.ui.actionAbortExperiment.setEnabled(True)
        # Disable experiment launching
        map(lambda action: action.setEnabled(False), self.experiments)

    def abort_experiment(self):
        """Aborts an experiment early.
        """
        assert self.worker is not None
        self.worker.abort()  # -> sig finished() from worker -> stop_experiment

    def stop_experiment(self):
        """Should be called when an experiment is over.
        Resets the worker thread (triggering immediate GC),
        Sets the status message in the status bar,
        and resets the toolbar.
        """
        self.worker = None
        map(lambda action: action.setEnabled(True), self.experiments)
        self.ui.actionAbortExperiment.setEnabled(False)
        self.set_status("Experiment ended.")

    # GUI utilities
    def set_status(self, text):
        """Sets the status message in the status bar.
        """
        self.status.setText(text)

    def warn(self, title, text):
        """Warn the user with a message box.
        """
        return MBox.Ok == MBox.warning(self, title, text,
                                       buttons=MBox.Ok | MBox.Cancel,
                                       defaultButton=MBox.Ok)

    def info(self, title, text):
        """Show a message box with information to the user.
        """
        MBox.information(self, title, text)

    def error(self, title, text):
        """Shows a critical error message to the user.
        """
        MBox.critical(self, title, text)

    def ask(self, title, text):
        """Asks a question in a message box to the user.
        Returns the answer (Ok => True, Cancel => False)
        """
        return MBox.Ok == MBox.question(self, title, text,
                                        buttons=MBox.Ok | MBox.Cancel,
                                        defaultButton=MBox.Ok)

    def shutter_warning(self, open=True):
        """Shows a modal dialog prompting the user to open / close the shutter.
        Returns True if the shutter was (presumably!) succesfully closed,
        and False if the user canceled the action.
        """
        text = "Please {} the shutter.".format("open" if open else "close")
        return self.warn("Shutter action", text)

    # Parameter tree utilities
    def set_gui_parameter(self, names, value):
        """Sets the value of a parameter in the parameter tree.
        Does not update the value in the (in-memory) configuration.
        Does not store the modified setting to disk.
        """
        self.params.child(*names).setValue(value)

    def exec_param_tree_action(self, name):
        """Executes an action from a button in the parameter tree.
        The parameter's name is given as an argument.
        This function dispatches behaviour based on this name.
        """
        if name == 'Choose export directory':
            self.choose_export_dir()
        else:
            self.error("Unknown action", "Unknown action button in parameter "
                       "tree: \"{}\".\nYou may want to add behaviour to this "
                       "button by modifying the function "
                       "exec_param_tree_action in main.py.".format(name))

    # EXPERIMENTS
    def experiment_HRS(self):
        """Starts HRS measurements.
        """
        if self.worker is not None:
            return self.info("Experiment running",
                             "An experiment is currently running, please wait")
        if self.servo is None or self.lockin is None:
            return self.error("Peripherals not found",
                              "Not all required peripherals are connected.")
        if not self.shutter_warning(open=True):
            return self.error("Aborting", "Aborting experiment.")

        self.start_experiment()
        self.set_status("Performing HRS measurements...")

        self.plotWidget.setRange(yRange=(-2.5, 2.5))
        self.plotWidget.setTitle('HRS')
        self.plotWidget.setLabel('left', text='Intensity', units='V')
        self.plotWidget.setLabel('bottom', text='Angle', units='&#x00b0;')

        # TODO
        self.data_tags = ("")
        self.data = []

        rstart = get_parameter('advanced', 'thetamax')
        self.worker = HRSWorker(
                lockin=self.lockin,
                servo=self.servo,
                poinst=get_parameter('hrs', 'nb_points'),
                measurements=get_parameter('hrs', 'nb_measures'),
                range=(rstart, rstart + 90 if rstart < 270 else rstart - 90))

        def handle_data_batch(x, y):
            self.data.append((x, y))
            self.ui.plotWidget.plot(data=np.array(self.data), clear=True,
                                    antialias=True)
        self.connect(self.worker, SIG("dataBatch(float, float)"),
                     handle_data_batch)

        self.connect(self.worker, SIG("progress(int)"), self.progress.setValue)
        self.connect(self.worker, SIG("status(QString)"), self.set_status)

        def finished():
            self.servo.goto(get_parameter('advanced', 'thetamax'), wait=False)
            self.stop_experiment()
            while not self.shutter_warning(open=False):
                pass
        self.connect(self.worker, SIG("finished()"), finished)

        self.worker.start()

    def experiment_depol(self):
        """Starts depolarization measurements.
        """
        if self.worker is not None:
            return self.info("Experiment running",
                             "An experiment is currently running, please wait")
        if self.servo is None or self.lockin is None:
            return self.error("Peripherals not found",
                              "Not all required peripherals are connected.")
        if not self.shutter_warning(open=True):
            return self.error("Aborting", "Aborting experiment.")

        self.start_experiment()
        self.set_status("Performing depolarization measurements...")

        # TODO
        self.plotWidget.setRange(yRange=(-2.5, 2.5))
        self.plotWidget.setTitle('Depolarization')
        self.plotWidget.setLabel('left', text='Intensity', units='V')
        self.plotWidget.setLabel('bottom', text='Angle', units='&#x00b0;')

        # TODO
        self.data_tags = ("")
        self.data = []

        # TODO
        rstart = get_parameter('advanced', 'thetamax')
        self.worker = HRSWorker(
                lockin=self.lockin,
                servo=self.servo,
                poinst=get_parameter('hrs', 'nb_points'),
                measurements=get_parameter('hrs', 'nb_measures'),
                range=(rstart, rstart + 90 if rstart < 270 else rstart - 90))

        # TODO
        def handle_data_batch(x, y):
            self.data.append((x, y))
            self.ui.plotWidget.plot(data=np.array(self.data), clear=True,
                                    antialias=True)
        self.connect(self.worker, SIG("dataBatch(float, float)"),
                     handle_data_batch)

        self.connect(self.worker, SIG("progress(int)"), self.progress.setValue)
        self.connect(self.worker, SIG("status(QString)"), self.set_status)

        def finished():
            self.servo.goto(get_parameter('advanced', 'thetamax'), wait=False)
            self.stop_experiment()
            while not self.shutter_warning(open=False):
                pass
        self.connect(self.worker, SIG("finished()"), finished)

        self.worker.start()
        pass

    def experiment_plot_test(self):
        """Starts a dummy experiment to test the plotting functionality.
        """
        if self.worker is not None:
            return self.info("Experiment running",
                             "An experiment is currently running, please wait")

        self.start_experiment()
        self.set_status("Performing test measurements...")

        self.plotWidget.setRange(xRange=(0, 4*np.pi), yRange=(-2.5, 2.5))
        self.plotWidget.setTitle('Plot test')
        self.plotWidget.setLabel('left', text='Y', units=None)
        self.plotWidget.setLabel('bottom', text='Angle', units='rad')

        self.data_tags = ('Theta', 'Y')
        self.data = []

        self.worker = SimpleWorker(60)

        def handle_data_batch(theta, y):
            self.data.append((theta, y))
            self.plotWidget.plot(np.array(self.data), clear=True,
                                 antialias=True)
        self.connect(self.worker, SIG("dataBatch(float, float)"),
                     handle_data_batch)

        self.connect(self.worker, SIG("progress(int)"), self.progress.setValue)
        self.connect(self.worker, SIG("status(QString)"), self.set_status)

        def finished():
            self.stop_experiment()
        self.connect(self.worker, SIG("finished()"), finished)

        self.worker.start()

    def experiment_servo_control(self):
        """Shows a small window to send commands to the servo.
        """
        if self.worker is not None:
            return self.info("Experiment running",
                             "An experiment is currently running, please wait")
        if self.servo is None:
            return self.error("Servo not connected",
                              "Servo not found - make sure it is connected.")
        self.start_experiment()
        self.set_status("Starting servo control...")

        popup = ServoControl(self.servo)
        popup.exec_()

        self.stop_experiment()

    def experiment_lockin_test(self):
        """Starts a dummy experiment to test the lock-in connection.
        """
        if self.worker is not None:
            return self.info("Experiment running",
                             "An experiment is currently running, please wait")
        if self.lockin is None:
            return self.error("Lock-in not connected",
                              "Lock-in not found - make sure it is connected")
        self.start_experiment()
        self.set_status("Starting lock-in control...")

        self.worker = LockInSponge(self.lockin)

        popup = LockInControl(self.lockin, self.worker, self)
        popup.exec_()

        self.stop_experiment()

    def experiment_full_sweep(self):
        if self.worker is not None:
            return self.info("Experiment running",
                             "An experiment is currently running, please wait")
        if self.lockin is None or self.servo is None:
            return self.error("Peripherals not found",
                              "Not all required peripherals are connected.")
        if not self.shutter_warning(open=True):
            return self.error("Aborting", "Aborting experiment.")

        self.start_experiment()
        self.set_status("Starting 360 degree sweep...")

        self.plotWidget.setRange(xRange=(0, 360))
        self.plotWidget.setTitle("Full sweep")
        self.plotWidget.setLabel('left', text='Intensity', units='V')
        self.plotWidget.setLabel('bottom', text='Angle', units='&#x00b0;')

        self.data_tags = ("Angle", "Intensity")
        self.data = []

        self.worker = SweepWorker(self.lockin, self.servo, 31, 3)
        self.connect(self.worker, SIG("progress(int)"), self.progress.setValue)
        self.connect(self.worker, SIG("status(QString)"), self.set_status)

        def handle_data(theta, y):
            self.data.append((theta, y))
            self.ui.plotWidget.plot(np.array(self.data), clear=True)
        self.connect(self.worker, SIG("dataBatch(float, float)"), handle_data)

        def finished():
            self.stop_experiment()
            while not self.shutter_warning(open=False):
                pass
        self.connect(self.worker, SIG("finished()"), finished)

        self.worker.start()


class ServoControl(QtGui.QDialog):
    def __init__(self, servo, parent=None):
        super(ServoControl, self).__init__()
        self.ui = Ui_ServoControl()
        self.ui.setupUi(self)
        self.setModal(True)
        self.setWindowTitle("Servo Control Window")
        self.show()
        assert servo is not None

        self.ui.doneButton.clicked.connect(lambda _: self.accept())
        self.ui.goButton.clicked.connect(
                lambda _: servo.goto(self.ui.targetSpinBox.value(),
                                     wait=False))
        self.ui.stopButton.clicked.connect(lambda _: servo.stop(wait=False))

        def jog(cw):
            servo.move(5 if cw else -5, wait=False)
        self.ui.jogCW.clicked.connect(lambda _: jog(True))
        self.ui.jogCCW.clicked.connect(lambda _: jog(False))
        timer = QtCore.QTimer(self)
        timer.timeout.connect(lambda: self.ui.lcd.display(servo.position()))
        timer.setInterval(100)  # update LCD every 100 milliseconds
        timer.start()


class LockInControl(QtGui.QDialog):
    def __init__(self, lockin, worker, parent=None):
        super(LockInControl, self).__init__()
        assert parent is not None
        self.ui = Ui_LockInControl()
        self.ui.setupUi(self)
        self.setModal(True)
        self.setWindowTitle("Lock-in control window")
        self.show()
        assert lockin is not None
        self.labels = [self.ui.l1, self.ui.l2, self.ui.l3, self.ui.l4,
                       self.ui.l5, self.ui.l6, self.ui.l7, self.ui.l8,
                       self.ui.l9, self.ui.l10, self.ui.l11]

        def accept(_):
            worker.abort()
            self.accept()
        self.ui.doneButton.clicked.connect(accept)

        def handle_data_batch(l):
            if len(l) < 11:
                print(l)
                return  # lock-in did not send enough data :/
            for i in range(11):
                self.labels[i].setText(str(l[i]))
        self.connect(worker, SIG("dataBatch(PyQt_PyObject)"),
                     handle_data_batch)

        def finished():
            parent.stop_experiment()
        self.connect(worker, SIG("finished()"), finished)

        worker.start()


if __name__ == "__main__":
    pg.systemInfo()
    import sys
    app = QtGui.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    app.exec_()
