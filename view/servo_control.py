# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'servo_control.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ServoControl(object):
    def setupUi(self, ServoControl):
        ServoControl.setObjectName(_fromUtf8("ServoControl"))
        ServoControl.resize(400, 220)
        self.gridLayout = QtGui.QGridLayout(ServoControl)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.groupBox = QtGui.QGroupBox(ServoControl)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout_2 = QtGui.QGridLayout(self.groupBox)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.goButton = QtGui.QPushButton(self.groupBox)
        self.goButton.setObjectName(_fromUtf8("goButton"))
        self.gridLayout_2.addWidget(self.goButton, 3, 0, 1, 1)
        self.targetSpinBox = QtGui.QSpinBox(self.groupBox)
        self.targetSpinBox.setMaximum(360)
        self.targetSpinBox.setObjectName(_fromUtf8("targetSpinBox"))
        self.gridLayout_2.addWidget(self.targetSpinBox, 1, 1, 1, 1)
        self.jogCCW = QtGui.QPushButton(self.groupBox)
        self.jogCCW.setObjectName(_fromUtf8("jogCCW"))
        self.gridLayout_2.addWidget(self.jogCCW, 5, 1, 1, 1)
        self.label = QtGui.QLabel(self.groupBox)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)
        self.lcd = QtGui.QLCDNumber(self.groupBox)
        self.lcd.setDigitCount(5)
        self.lcd.setSegmentStyle(QtGui.QLCDNumber.Filled)
        self.lcd.setObjectName(_fromUtf8("lcd"))
        self.gridLayout_2.addWidget(self.lcd, 0, 1, 1, 1)
        self.stopButton = QtGui.QPushButton(self.groupBox)
        self.stopButton.setObjectName(_fromUtf8("stopButton"))
        self.gridLayout_2.addWidget(self.stopButton, 3, 1, 1, 1)
        self.jogCW = QtGui.QPushButton(self.groupBox)
        self.jogCW.setObjectName(_fromUtf8("jogCW"))
        self.gridLayout_2.addWidget(self.jogCW, 5, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBox, 0, 0, 1, 1)
        self.doneButton = QtGui.QPushButton(ServoControl)
        self.doneButton.setObjectName(_fromUtf8("doneButton"))
        self.gridLayout.addWidget(self.doneButton, 1, 0, 1, 1)

        self.retranslateUi(ServoControl)
        QtCore.QMetaObject.connectSlotsByName(ServoControl)

    def retranslateUi(self, ServoControl):
        ServoControl.setWindowTitle(_translate("ServoControl", "Servo Control Window", None))
        self.groupBox.setTitle(_translate("ServoControl", "Servo Control", None))
        self.goButton.setText(_translate("ServoControl", "Go", None))
        self.targetSpinBox.setSuffix(_translate("ServoControl", " degrees", None))
        self.jogCCW.setText(_translate("ServoControl", "Jog Counterclockwise", None))
        self.label.setText(_translate("ServoControl", "Current position:", None))
        self.label_2.setText(_translate("ServoControl", "Go to:", None))
        self.stopButton.setText(_translate("ServoControl", "Stop", None))
        self.jogCW.setText(_translate("ServoControl", "Jog Clockwise", None))
        self.doneButton.setText(_translate("ServoControl", "Done", None))

