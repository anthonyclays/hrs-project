# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'lockin_control.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_LockInControl(object):
    def setupUi(self, LockInControl):
        LockInControl.setObjectName(_fromUtf8("LockInControl"))
        LockInControl.resize(266, 352)
        self.gridLayout = QtGui.QGridLayout(LockInControl)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.doneButton = QtGui.QPushButton(LockInControl)
        self.doneButton.setObjectName(_fromUtf8("doneButton"))
        self.gridLayout.addWidget(self.doneButton, 2, 0, 1, 1)
        self.groupBox = QtGui.QGroupBox(LockInControl)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.formLayout = QtGui.QFormLayout(self.groupBox)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_1 = QtGui.QLabel(self.groupBox)
        self.label_1.setObjectName(_fromUtf8("label_1"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_1)
        self.l1 = QtGui.QLabel(self.groupBox)
        self.l1.setObjectName(_fromUtf8("l1"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.l1)
        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_2)
        self.l2 = QtGui.QLabel(self.groupBox)
        self.l2.setObjectName(_fromUtf8("l2"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.l2)
        self.label_3 = QtGui.QLabel(self.groupBox)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_3)
        self.l3 = QtGui.QLabel(self.groupBox)
        self.l3.setObjectName(_fromUtf8("l3"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.l3)
        self.label_4 = QtGui.QLabel(self.groupBox)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_4)
        self.l4 = QtGui.QLabel(self.groupBox)
        self.l4.setObjectName(_fromUtf8("l4"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.l4)
        self.label_5 = QtGui.QLabel(self.groupBox)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.label_5)
        self.l5 = QtGui.QLabel(self.groupBox)
        self.l5.setObjectName(_fromUtf8("l5"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.l5)
        self.label_6 = QtGui.QLabel(self.groupBox)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.LabelRole, self.label_6)
        self.l6 = QtGui.QLabel(self.groupBox)
        self.l6.setObjectName(_fromUtf8("l6"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.FieldRole, self.l6)
        self.label_7 = QtGui.QLabel(self.groupBox)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.formLayout.setWidget(6, QtGui.QFormLayout.LabelRole, self.label_7)
        self.l7 = QtGui.QLabel(self.groupBox)
        self.l7.setObjectName(_fromUtf8("l7"))
        self.formLayout.setWidget(6, QtGui.QFormLayout.FieldRole, self.l7)
        self.label_8 = QtGui.QLabel(self.groupBox)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.formLayout.setWidget(7, QtGui.QFormLayout.LabelRole, self.label_8)
        self.l8 = QtGui.QLabel(self.groupBox)
        self.l8.setObjectName(_fromUtf8("l8"))
        self.formLayout.setWidget(7, QtGui.QFormLayout.FieldRole, self.l8)
        self.label_9 = QtGui.QLabel(self.groupBox)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.formLayout.setWidget(8, QtGui.QFormLayout.LabelRole, self.label_9)
        self.l9 = QtGui.QLabel(self.groupBox)
        self.l9.setObjectName(_fromUtf8("l9"))
        self.formLayout.setWidget(8, QtGui.QFormLayout.FieldRole, self.l9)
        self.label_10 = QtGui.QLabel(self.groupBox)
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.formLayout.setWidget(9, QtGui.QFormLayout.LabelRole, self.label_10)
        self.l10 = QtGui.QLabel(self.groupBox)
        self.l10.setObjectName(_fromUtf8("l10"))
        self.formLayout.setWidget(9, QtGui.QFormLayout.FieldRole, self.l10)
        self.label_11 = QtGui.QLabel(self.groupBox)
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.formLayout.setWidget(10, QtGui.QFormLayout.LabelRole, self.label_11)
        self.l11 = QtGui.QLabel(self.groupBox)
        self.l11.setObjectName(_fromUtf8("l11"))
        self.formLayout.setWidget(10, QtGui.QFormLayout.FieldRole, self.l11)
        self.gridLayout.addWidget(self.groupBox, 1, 0, 1, 1)

        self.retranslateUi(LockInControl)
        QtCore.QMetaObject.connectSlotsByName(LockInControl)

    def retranslateUi(self, LockInControl):
        LockInControl.setWindowTitle(_translate("LockInControl", "Form", None))
        self.doneButton.setText(_translate("LockInControl", "Done", None))
        self.groupBox.setTitle(_translate("LockInControl", "Lock-in control", None))
        self.label_1.setText(_translate("LockInControl", "1:", None))
        self.l1.setText(_translate("LockInControl", "0", None))
        self.label_2.setText(_translate("LockInControl", "2:", None))
        self.l2.setText(_translate("LockInControl", "0", None))
        self.label_3.setText(_translate("LockInControl", "3:", None))
        self.l3.setText(_translate("LockInControl", "0", None))
        self.label_4.setText(_translate("LockInControl", "4:", None))
        self.l4.setText(_translate("LockInControl", "0", None))
        self.label_5.setText(_translate("LockInControl", "5:", None))
        self.l5.setText(_translate("LockInControl", "0", None))
        self.label_6.setText(_translate("LockInControl", "6:", None))
        self.l6.setText(_translate("LockInControl", "0", None))
        self.label_7.setText(_translate("LockInControl", "7:", None))
        self.l7.setText(_translate("LockInControl", "0", None))
        self.label_8.setText(_translate("LockInControl", "8:", None))
        self.l8.setText(_translate("LockInControl", "0", None))
        self.label_9.setText(_translate("LockInControl", "9:", None))
        self.l9.setText(_translate("LockInControl", "0", None))
        self.label_10.setText(_translate("LockInControl", "10:", None))
        self.l10.setText(_translate("LockInControl", "0", None))
        self.label_11.setText(_translate("LockInControl", "11:", None))
        self.l11.setText(_translate("LockInControl", "0", None))

