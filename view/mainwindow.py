# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(800, 600)
        self.centralWidget = QtGui.QWidget(MainWindow)
        self.centralWidget.setObjectName(_fromUtf8("centralWidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralWidget)
        self.gridLayout.setMargin(11)
        self.gridLayout.setSpacing(6)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.splitter = QtGui.QSplitter(self.centralWidget)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.plotWidget = PlotWidget(self.splitter)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(5)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.plotWidget.sizePolicy().hasHeightForWidth())
        self.plotWidget.setSizePolicy(sizePolicy)
        self.plotWidget.setObjectName(_fromUtf8("plotWidget"))
        self.paramTree = ParameterTree(self.splitter)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(3)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.paramTree.sizePolicy().hasHeightForWidth())
        self.paramTree.setSizePolicy(sizePolicy)
        self.paramTree.setObjectName(_fromUtf8("paramTree"))
        self.paramTree.headerItem().setText(0, _fromUtf8("1"))
        self.gridLayout.addWidget(self.splitter, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtGui.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 800, 19))
        self.menuBar.setObjectName(_fromUtf8("menuBar"))
        self.menu_File = QtGui.QMenu(self.menuBar)
        self.menu_File.setObjectName(_fromUtf8("menu_File"))
        self.menu_Help = QtGui.QMenu(self.menuBar)
        self.menu_Help.setObjectName(_fromUtf8("menu_Help"))
        self.menu_Settings = QtGui.QMenu(self.menuBar)
        self.menu_Settings.setObjectName(_fromUtf8("menu_Settings"))
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QtGui.QToolBar(MainWindow)
        self.mainToolBar.setAllowedAreas(QtCore.Qt.LeftToolBarArea|QtCore.Qt.RightToolBarArea|QtCore.Qt.TopToolBarArea)
        self.mainToolBar.setObjectName(_fromUtf8("mainToolBar"))
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtGui.QStatusBar(MainWindow)
        self.statusBar.setObjectName(_fromUtf8("statusBar"))
        MainWindow.setStatusBar(self.statusBar)
        self.actionQuit = QtGui.QAction(MainWindow)
        self.actionQuit.setObjectName(_fromUtf8("actionQuit"))
        self.actionSaveParameters = QtGui.QAction(MainWindow)
        self.actionSaveParameters.setObjectName(_fromUtf8("actionSaveParameters"))
        self.actionExport = QtGui.QAction(MainWindow)
        self.actionExport.setObjectName(_fromUtf8("actionExport"))
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName(_fromUtf8("actionAbout"))
        self.actionViewManual = QtGui.QAction(MainWindow)
        self.actionViewManual.setObjectName(_fromUtf8("actionViewManual"))
        self.actionStartHRS = QtGui.QAction(MainWindow)
        self.actionStartHRS.setObjectName(_fromUtf8("actionStartHRS"))
        self.actionLoadParameters = QtGui.QAction(MainWindow)
        self.actionLoadParameters.setObjectName(_fromUtf8("actionLoadParameters"))
        self.actionPlotTest = QtGui.QAction(MainWindow)
        self.actionPlotTest.setObjectName(_fromUtf8("actionPlotTest"))
        self.actionChooseExportDirectory = QtGui.QAction(MainWindow)
        self.actionChooseExportDirectory.setObjectName(_fromUtf8("actionChooseExportDirectory"))
        self.actionAbortExperiment = QtGui.QAction(MainWindow)
        self.actionAbortExperiment.setEnabled(False)
        self.actionAbortExperiment.setObjectName(_fromUtf8("actionAbortExperiment"))
        self.actionServoControl = QtGui.QAction(MainWindow)
        self.actionServoControl.setObjectName(_fromUtf8("actionServoControl"))
        self.actionLockinTest = QtGui.QAction(MainWindow)
        self.actionLockinTest.setObjectName(_fromUtf8("actionLockinTest"))
        self.actionFullSweep = QtGui.QAction(MainWindow)
        self.actionFullSweep.setObjectName(_fromUtf8("actionFullSweep"))
        self.actionStartDepol = QtGui.QAction(MainWindow)
        self.actionStartDepol.setObjectName(_fromUtf8("actionStartDepol"))
        self.menu_File.addAction(self.actionQuit)
        self.menu_File.addAction(self.actionExport)
        self.menu_Help.addAction(self.actionAbout)
        self.menu_Help.addAction(self.actionViewManual)
        self.menu_Settings.addAction(self.actionSaveParameters)
        self.menu_Settings.addAction(self.actionLoadParameters)
        self.menu_Settings.addSeparator()
        self.menu_Settings.addAction(self.actionChooseExportDirectory)
        self.menuBar.addAction(self.menu_File.menuAction())
        self.menuBar.addAction(self.menu_Settings.menuAction())
        self.menuBar.addAction(self.menu_Help.menuAction())
        self.mainToolBar.addAction(self.actionStartHRS)
        self.mainToolBar.addAction(self.actionStartDepol)
        self.mainToolBar.addAction(self.actionPlotTest)
        self.mainToolBar.addAction(self.actionServoControl)
        self.mainToolBar.addAction(self.actionLockinTest)
        self.mainToolBar.addAction(self.actionFullSweep)
        self.mainToolBar.addAction(self.actionAbortExperiment)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "TODO Window Title", None))
        self.menu_File.setTitle(_translate("MainWindow", "&File", None))
        self.menu_Help.setTitle(_translate("MainWindow", "&Help", None))
        self.menu_Settings.setTitle(_translate("MainWindow", "Setti&ngs", None))
        self.actionQuit.setText(_translate("MainWindow", "&Quit", None))
        self.actionQuit.setShortcut(_translate("MainWindow", "Ctrl+Q", None))
        self.actionSaveParameters.setText(_translate("MainWindow", "&Save parameters", None))
        self.actionExport.setText(_translate("MainWindow", "&Export measurements...", None))
        self.actionExport.setShortcut(_translate("MainWindow", "Ctrl+E", None))
        self.actionAbout.setText(_translate("MainWindow", "&About...", None))
        self.actionAbout.setShortcut(_translate("MainWindow", "F11", None))
        self.actionViewManual.setText(_translate("MainWindow", "&View manual", None))
        self.actionViewManual.setShortcut(_translate("MainWindow", "F1", None))
        self.actionStartHRS.setText(_translate("MainWindow", "Start HRS", None))
        self.actionStartHRS.setToolTip(_translate("MainWindow", "Start HRS measurements", None))
        self.actionLoadParameters.setText(_translate("MainWindow", "&Load parameters", None))
        self.actionPlotTest.setText(_translate("MainWindow", "Start plot test", None))
        self.actionChooseExportDirectory.setText(_translate("MainWindow", "&Choose export directory...", None))
        self.actionAbortExperiment.setText(_translate("MainWindow", "Abort experiment", None))
        self.actionAbortExperiment.setToolTip(_translate("MainWindow", "Abort current experiment.", None))
        self.actionAbortExperiment.setShortcut(_translate("MainWindow", "Ctrl+X", None))
        self.actionServoControl.setText(_translate("MainWindow", "Start servo control", None))
        self.actionServoControl.setToolTip(_translate("MainWindow", "Start servo control", None))
        self.actionLockinTest.setText(_translate("MainWindow", "Start lock-in test", None))
        self.actionFullSweep.setText(_translate("MainWindow", "Full sweep", None))
        self.actionStartDepol.setText(_translate("MainWindow", "Start depolarization", None))

from pyqtgraph import PlotWidget
from pyqtgraph.parametertree import ParameterTree
